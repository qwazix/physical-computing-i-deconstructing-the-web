



/* ControlTag Loader for Protothema 5bfbaea3-3ddd-4b98-92b8-7c01261b708e */
(function(w, cs) {
  
  if (/Twitter for iPhone/.test(w.navigator.userAgent || '')) {
    return;
  }

  var debugging = /kxdebug/.test(w.location);
  var log = function() {
    
    debugging && w.console && w.console.log([].slice.call(arguments).join(' '));
  };

  var load = function(url, callback) {
    log('Loading script from:', url);
    var node = w.document.createElement('script');
    node.async = true;  
    node.src = url;

    
    node.onload = node.onreadystatechange = function () {
      var state = node.readyState;
      if (!callback.done && (!state || /loaded|complete/.test(state))) {
        log('Script loaded from:', url);
        callback.done = true;  
        callback();
      }
    };

    
    var sibling = w.document.getElementsByTagName('script')[0];
    sibling.parentNode.insertBefore(node, sibling);
  };

  var config = {"app":{"name":"krux-scala-config-webservice","version":"3.42.1","schema_version":3},"confid":"uvtt414qn","context_terms":[],"publisher":{"name":"Protothema","active":true,"uuid":"5bfbaea3-3ddd-4b98-92b8-7c01261b708e","version_bucket":"stable","id":5017},"params":{"link_header_bidder":false,"site_level_supertag_config":"site","recommend":false,"control_tag_pixel_throttle":100,"fingerprint":false,"optout_button_optout_text":"Browser Opt Out","user_data_timing":"load","consent_active":true,"use_central_usermatch":true,"store_realtime_segments":false,"tag_source":false,"link_hb_start_event":"ready","optout_button_optin_text":"Browser Opt In","link_hb_timeout":2000,"link_hb_adserver_subordinate":true,"optimize_realtime_segments":false,"link_hb_adserver":"dfp","target_fingerprint":false,"context_terms":true,"optout_button_id":"kx-optout-button","dfp_premium":true},"prioritized_segments":[],"realtime_segments":[],"services":{"userdata":"//cdn.krxd.net/userdata/get","contentConnector":"https://connector.krxd.net/content_connector","stats":"//apiservices.krxd.net/stats","optout":"//cdn.krxd.net/userdata/optout/status","event":"//beacon.krxd.net/event.gif","set_optout":"https://consumer.krxd.net/consumer/optout","data":"//beacon.krxd.net/data.gif","link_hb_stats":"//beacon.krxd.net/link_bidder_stats.gif","userData":"//cdn.krxd.net/userdata/get","link_hb_mas":"https://link.krxd.net/hb","config":"//cdn.krxd.net/controltag/{{ confid }}.js","social":"//beacon.krxd.net/social.gif","addSegment":"//cdn.krxd.net/userdata/add","pixel":"//beacon.krxd.net/pixel.gif","um":"https://usermatch.krxd.net/um/v2","controltag":"//cdn.krxd.net/ctjs/controltag.js.{hash}","loopback":"https://consumer.krxd.net/consumer/tmp_cookie","remove":"https://consumer.krxd.net/consumer/remove/5bfbaea3-3ddd-4b98-92b8-7c01261b708e","click":"https://apiservices.krxd.net/click_tracker/track","stats_export":"//beacon.krxd.net/controltag_stats.gif","userdataApi":"//cdn.krxd.net/userdata/v1/segments/get","cookie":"//beacon.krxd.net/cookie2json","proxy":"//cdn.krxd.net/partnerjs/xdi","consent_get":"https://consumer.krxd.net/consent/get/5bfbaea3-3ddd-4b98-92b8-7c01261b708e","consent_set":"https://consumer.krxd.net/consent/set/5bfbaea3-3ddd-4b98-92b8-7c01261b708e","is_optout":"https://beacon.krxd.net/optout_check","impression":"//beacon.krxd.net/ad_impression.gif","transaction":"//beacon.krxd.net/transaction.gif","log":"//jslog.krxd.net/jslog.gif","portability":"https://consumer.krxd.net/consumer/portability/5bfbaea3-3ddd-4b98-92b8-7c01261b708e","set_optin":"https://consumer.krxd.net/consumer/optin","usermatch":"//beacon.krxd.net/usermatch.gif"},"experiments":[],"site":{"name":"Iefimerida.gr","cap":255,"id":1682884,"organization_id":5017,"uid":"uvtt414qn"},"tags":[{"id":43332,"name":"Standard DTC","content":"<script>\n(function(){\n\tKrux('scrape',{'page_attr_url_path_1':{'url_path':'1'}});\n\tKrux('scrape',{'page_attr_url_path_2':{'url_path':'2'}});\n\tKrux('scrape',{'page_attr_url_path_3':{'url_path':'3'}});\n  \tKrux('scrape',{'page_attr_url_path_4':{'url_path':'4'}});\n\tKrux('scrape',{'page_attr_meta_keywords':{meta_name:'keywords'}});\n\n\tKrux('scrape',{'page_attr_domain':{url_domain: '2'}});\n\n})();\n</script>","target":null,"target_action":"append","timing":"onready","method":"document","priority":null,"template_replacement":true,"internal":true,"criteria":[],"collects_data":true},{"id":43421,"name":"UTM DTC","content":"<script>\n(function(){\n\n\tvar params = Krux('require:util').urlParams();\n\t\n\tKrux ('set', { \n\t'page_attr_utm_source': params.utm_source,\n\t'page_attr_utm_medium': params.utm_medium,\n\t'page_attr_utm_campaign': params.utm_campaign,\n\t'page_attr_utm_content': params.utm_content,\n\t'page_attr_utm_term': params.utm_term \n\t});\n\t\n})();\n</script>","target":null,"target_action":"append","timing":"onload","method":"document","priority":null,"template_replacement":true,"internal":true,"criteria":[],"collects_data":true},{"id":43434,"name":"DTC - Generic Scraper (subdomain)","content":"<script>\n(function() {\n    /* Generic Scrape Tag - Configurable Data Collection */\n    var _, filterValues, libUtil, prefix, scrapeConfig, toSet;\n    _ = Krux('require:underscore');\n    libUtil = Krux('require:util.library-tag');\n    toSet = {};\n    filterValues = function(val) {\n        var i, item, len, ref, str, x;\n        if (_.isArray(val)) {\n            return _.compact((function() {\n                var i, len, results;\n                results = [];\n                for (i = 0, len = val.length; i < len; i++) {\n                    item = val[i];\n                    results.push(filterValues(item));\n                }\n                return results;\n            })());\n        }\n        str = \"\" + val;\n        if (!((val != null) && str.length > 0)) {\n            return;\n        }\n        ref = libUtil.EXCLUDE_VALUES_CONFIG;\n        for (i = 0, len = ref.length; i < len; i++) {\n            x = ref[i];\n            if (str.match(x) != null) {\n                return;\n            }\n        }\n        return val;\n    };\n    scrapeConfig = function(config, type) {\n        var attr, i, len, parts, ref, results, value;\n        ref = libUtil.removeFalsyStrings(config);\n        results = [];\n        for (i = 0, len = ref.length; i < len; i++) {\n            attr = ref[i];\n            parts = attr.split('|');\n            if (parts.length === 1 && 'js_global'.match(/(dom|javascript)/)) {\n                continue;\n            }\n            if (parts.length === 1) {\n                parts.unshift(libUtil.normalizeAttrName(parts[0]));\n            }\n            switch ('js_global') {\n                case 'get':\n                    value = filterValues(Krux('get', parts[1]));\n                    break;\n                default:\n                    value = filterValues(Krux('scrape.js_global', parts[1]));\n            }\n            if (value) {\n                results.push(toSet[type + \"_attr_\" + parts[0]] = value);\n            } else {\n                results.push(void 0);\n            }\n        }\n        return results;\n    };\n    scrapeConfig('subdomain|window.location.host', 'page');\n    scrapeConfig('undefined', 'user');\n    prefix = libUtil.resolvePrefix('none', 'undefined',\n        'undefined');\n    toSet = Krux('prefix:attr', toSet, prefix);\n    Krux('set', toSet);\n}).call();\n</script>","target":null,"target_action":"append","timing":"onload","method":"document","priority":null,"template_replacement":true,"internal":true,"criteria":[],"collects_data":true},{"id":43456,"name":"Iefimerida Facebook","content":"<script>\n(function() {\n    var c = Krux;\n    function n() {\n        var e = c('require:underscore');\n        var t = \"false\" === \"true\";\n        var n = c('get', atob('dXNlcl9zZWdtZW50cw==')) || [];\n        var i = e.without('uxzn22x35:Iefimerida_Test_Segment'.split(',').map(function(e) {\n            var t = e.split(':');\n            if (t.length == 2 && ~n.indexOf(t[0])) {\n                return t[1];\n            }\n        }), undefined);\n        if (t || i.length) {\n            !function(f,b,e,v,n,t,s)\n            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?\n            n.callMethod.apply(n,arguments):n.queue.push(arguments)};\n            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.agent='tmsalesforce';n.version='2.0';\n            n.queue=[];t=b.createElement(e);t.async=!0;\n            t.src=v;s=b.getElementsByTagName(e)[0];\n            s.parentNode.insertBefore(t,s)}(window, document,'script',\n            'https://connect.facebook.net/en_US/fbevents.js');\n            fbq('init', '590266244822380');\n            fbq('track', 'PageView');\n\n            var r = 0, a = i.length;\n            for (;r < a; r++) {\n                fbq('trackSingle', '590266244822380', 'ViewContent', {\n                    content_name: i[r]\n                });\n            }\n        }\n    }\n    if (c('get', 'config').params.consent_active) {\n        c('consent:get', function(e, t) {\n            if (t.settings.tg) {\n                n();\n            }\n        });\n    } else {\n        n();\n    }\n}).call();\n</script>","target":null,"target_action":"append","timing":"onload","method":"document","priority":null,"template_replacement":true,"internal":false,"criteria":[],"collects_data":true}],"usermatch_tags":[{"id":6,"name":"Google User Match","content":"<script>\r\n(function() {\r\n\r\nvar kuid = Krux('get', 'user');\r\n  if(kuid){\r\n  // original google user match tag. will be deprecated june 1, 2020\r\n  new Image().src = 'https://usermatch.krxd.net/um/v2?partner=google';\r\n\r\n  // new google user match where they host the match table. The KUID needs to be base64 encoded, but the ids sent will be regular kuids\r\n  var baseEncodedKuid = btoa(kuid).replace(/=$/, '');\r\n  new Image().src = 'https://cm.g.doubleclick.net/pixel?google_nid=krux_digital&google_hm='+baseEncodedKuid;\r\n  }\r\n\r\n})();\r\n</script>","target":null,"target_action":"append","timing":"onload","method":"document","priority":1,"template_replacement":false,"internal":true,"criteria":[],"collects_data":true}],"link":{"adslots":{},"bidders":{}}};
  
  for (var i = 0, tags = config.tags, len = tags.length, tag; (tag = tags[i]); ++i) {
    if (String(tag.id) in cs) {
      tag.content = cs[tag.id];
    }
  }

  
  var esiGeo = String(function(){/*
   <esi:include src="/geoip_esi"/>
  */}).replace(/^.*\/\*[^{]+|[^}]+\*\/.*$/g, '');

  if (esiGeo) {
    log('Got a request for:', esiGeo, 'adding geo to config.');
    try {
      config.geo = w.JSON.parse(esiGeo);
    } catch (__) {
      
      log('Unable to parse geo from:', config.geo);
      config.geo = {};
    }
  }



  var proxy = (window.Krux && window.Krux.q && window.Krux.q[0] && window.Krux.q[0][0] === 'proxy');

  if (!proxy || true) {
    

  load('//cdn.krxd.net/ctjs/controltag.js.840d44399e357e7da3f94ce724fcd35c', function() {
    log('Loaded stable controltag resource');
    Krux('config', config);
  });

  }

})(window, (function() {
  var obj = {};
  
  return obj;
})());
