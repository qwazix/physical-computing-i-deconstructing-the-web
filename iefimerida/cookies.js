let oldClientX = -1;
let oldClientY = -1;
let noCookieIntval;

(() => {
    // constants
    const COLOR = {
      WINDOW: '#f8f9fa',
      BAR: '#868e96',
      DOT: '#f8f9fa',
      HEADER: '#868e96',
      TEXT: '#ced4da',
      SOCIAL: '#f06595',
      IMAGE: '#22b8cf'
    };
  
    // variables
    let canvas, elements, engine;
  
    function init() {
      // engine
      engine = Matter.Engine.create();
      engine.world.gravity.y = 0.5;
  
      // render
      let render = Matter.Render.create({
        element: document.getElementById('cookie-canvas'),
        engine: engine,
        options: {
          width: window.innerWidth,
          height: window.innerHeight,
          wireframes: false, // need this or various render styles won't take
          background: "transparent"
        }
      });
      Matter.Render.run(render);
  
      // runner
      let runner = Matter.Runner.create();
      Matter.Runner.run(runner, engine);
  
      // fixed bodies
      Matter.World.add(engine.world, [
        // boundaries (top, bottom, left, right)
        wall(window.innerWidth/2, -10, window.innerWidth, 20),
        wall(window.innerWidth/2, window.innerHeight+10, window.innerWidth, 20),
        wall(-10, window.innerHeight/2, 20, window.innerHeight),
        wall(window.innerWidth+10, window.innerHeight/2, 20, window.innerHeight),
  
      ]);
  
      // bodies to toss around

      Matter.World.add(engine.world, elements);
  
      canvas = document.querySelector('#cookie-canvas canvas');
      canvas.style.display = "none"
      run();
      document.getElementById("cookie-popup").style.display = "none";
    }
  
    function run() {
      canvas.classList.add('slam');
      slam();
      canvas.style.display = "block";
    //   setTimeout(slam, 2000);
    }
  
    function slam() {
      // let the bodies hit the floor
      elements.forEach((body) => {
        Matter.Body.setStatic(body, false);
        Matter.Body.setVelocity(body, {
          x: rand(-4, 4),
          y: rand(-6, -4)
        });
        Matter.Body.setAngularVelocity(body, rand(-0.05, 0.05));
      });
  
      // repeat
      canvas.classList.remove('slam');
      setTimeout(run, 5000);
    }
  
    // matter.js has a built in random range function, but it is deterministic
    function rand(min, max) {
      return Math.random() * (max - min) + min;
    }
  
    function wall(x, y, width, height) {
      return Matter.Bodies.rectangle(x, y, width, height,  {
        isStatic: true,
        render: { visible: false }
      });
    }

    function cookie(x, y, radius, color) {
        return Matter.Bodies.circle(x, y, radius, {
          isStatic: true,
          restitution: 1,
          render: {
                sprite: {
                    texture: './cookie.svg'
                }
            }
        });
      }
  
    // window.addEventListener('load', init, false);


    document.getElementById("yaycookies").onclick = function(){

      document.querySelectorAll("audio").forEach( item => {
        item.pause()
      } )


        elements = [
            cookie(window.innerWidth/2, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+30, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+60, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+90, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+30, window.innerHeight/2-20, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2-30, window.innerHeight/2-30, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+10, window.innerHeight/2-30, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+30, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+60, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+90, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+30, window.innerHeight/2-20, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2-30, window.innerHeight/2-30, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+10, window.innerHeight/2-30, 20, COLOR.SOCIAL),
          ];
        // run();
        if (engine){
          Matter.World.add(engine.world, elements);
          slam();
          document.getElementById("cookie-popup").style.display = "none";
          clearInterval(noCookieIntval);
        } else {
          init();
        }
        
    }
    document.getElementById("nocookies").onclick = function(){
      document.querySelectorAll("audio").forEach( item => {
        item.play()
      } )


        // document.getElementById("cookie-popup").style.display = "none";
        elements = [
            cookie(window.innerWidth/2, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+30, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+60, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+90, window.innerHeight/2, 20, COLOR.SOCIAL),
            cookie(window.innerWidth/2+30, window.innerHeight/2-20, 20, COLOR.SOCIAL),
          ];
          init();

          let supercookie = document.getElementById("supercookie")
          // supercookie.style.display = "block";
          window.addEventListener("mousemove", function(e){
            if (oldClientX == -1){
              supercookie.style.left = e.pageX + 10 + "px"
              supercookie.style.top = e.pageY + 10 + "px"
            } else {
              supercookie.style.left = oldClientX + 10 + "px"
              supercookie.style.top = oldClientY + 10 + "px"
            }
            supercookie.style.display = "block";
            setTimeout(function(){
              oldClientX = e.pageX
              oldClientY = e.pageY
            }, 400);
          })

          this.onclick = function(){
            document.getElementById("cookie-popup").style.display = "none";
          }
          
        noCookieIntval = setInterval(function(){
            document.getElementById("cookie-popup").style.display = "block";
        },20000)
    }

  })();
  
  window.addEventListener("mousemove", function(e){
    let mute = document.getElementById("mute");
    if ((Math.abs(mute.offsetLeft+40 - e.pageX) < 50 )&& (Math.abs(mute.offsetTop+20 - e.pageY)<50)){
      let m = (Math.random() - 0.5 > 0 ? -1 : 1);
      console.log(m)
      mute.style.left = e.pageX + m*80 + "px" 
      mute.style.top = e.pageY + m*80 + "px"
    }
  })

 document.querySelectorAll("a:not(.qwazix)").forEach(link => {
   link.addEventListener("click", function(e){
     alert("FAKE NEWS!")
     e.preventDefault();
   })
 });

 var firstTimeNotifs = true;
 var firstTimeNewsletter = true;

 document.addEventListener("scroll", function(e){

   if (window.scrollY > 1500 && firstTimeNotifs){
    confirm("Please allow notifications");
    firstTimeNotifs = false;
   }
   
   if (window.scrollY > 3000 && firstTimeNewsletter){
    prompt("Enter your email address so we can send you spam");
    firstTimeNewsletter = false;
   }

 })